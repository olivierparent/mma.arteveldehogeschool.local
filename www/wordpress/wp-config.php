<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mma_arteveldehogeschool_be');

/** MySQL database username */
define('DB_USER', 'mma_db_user');

/** MySQL database password */
define('DB_PASSWORD', 'mma_db_password');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'JJOlH}nxI_$1p(]ra/U}2^rV/DQZxt3VY,}APLw/9xz`l<2(9wj|ZM`(h 6SV=C<');
define('SECURE_AUTH_KEY',  'U~!a5cH3S3wf)(dHo.aK#+96B@0y&N=_k-IwiS;_zl*n:WiMXLfDh2Qqg`SdhlNV');
define('LOGGED_IN_KEY',    'uo O{zLOMTC-pbsJTkF^TaMdyb]He{$,tv]{Rg]7psAp|t0oaEG/;|HP5xM.C>$c');
define('NONCE_KEY',        '%7p`_]6Wpb(buB5+(ScGDm}a T]wS5G[|GdX+<b*eT7-tu}tX VcAl./-r0: sU4');
define('AUTH_SALT',        'gKC+w-0=9CRS[+Lz |]x>#dVU]Wa45{4Efr{~nnQGz :_XXA2?k99$G|=rr^.I(_');
define('SECURE_AUTH_SALT', '|}evA*+!/wm&3-j$:`PnxOs|v1`O/5j#w>.6k-gn+wM_r7ny(!n=DC`O=JUMR~p^');
define('LOGGED_IN_SALT',   'o%3N867 <%O,w,+?3$4,h=}-<VBv(*SDF-M2~(&!kn|Z(hP E,dJY|[+L~=e{2f~');
define('NONCE_SALT',       'U-B:/~o]~GO;To<yZo-[m!{C<Y=tI,{x+`31:1MO-WY5G!dt-JHaM->qt0J.Y?I#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
