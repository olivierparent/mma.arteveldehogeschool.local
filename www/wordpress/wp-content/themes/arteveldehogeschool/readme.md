Compile Less
============

 - Download and install [Koala](http://koala-app.com).
 - Drag and drop `css` folder into Koala
 - In Koala: **Settings** → **LESS**:
    - Check **Source Map**
    - **Output Style:** `compress`
